import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { ICreateCard } from '../type/typeProps';

const Card = ({ card, opened, successed }: ICreateCard) => {

  const [classSizeCard, setClassSizeCard] = useState<string>('card__baby');
  const [classStatusCard, setClassStatusCard] = useState<string>('card__cover');
  const [open, setOpen] = useState(opened);
  const dispatch = useDispatch();

  // console.log(card.id);
  // console.log(opened);
  // console.log(successed);
  
  // Изменение размера карточки в зависимости от сложности
  useEffect(() => {
    setClassSizeCard(`card__${card.dificulty}`);
  }, [card.dificulty]);

  // Изменение статуса карточки
  useEffect(() => {
      if (opened) {
        setClassStatusCard('card__pair');
        setOpen(true);
      };
      if (successed) {
        setClassStatusCard('card__success');
        setOpen(false);
      };
      if (!opened && !successed) {
        setClassStatusCard('card__cover');
        setOpen(false);
      };
    },[opened, successed]);

  const handleClickSetCard = () => {
    // Проверка на двойное нажатие
    if (opened) {
      throw new TypeError('Вы уже открыли эту карту');
    };
    dispatch({ type: 'SET_OPENED_CARD', id: card.id, cardNumber: card.cardNumber });
    dispatch({ type: 'SUCCESSED_PAIR' });
  }

  return (
    <>
      <button id={`${card.id}`} className={classSizeCard + ' ' + classStatusCard} onClick={handleClickSetCard} disabled={successed}>
        {open ? (card.img === '' ?
          <p>{`${card.cardNumber}`}</p>
          :
          <img src={card.img} alt='картинка' className='card__img'></img>)
          :
          null
        }
      </button>
    </>
  )
}

export default Card;